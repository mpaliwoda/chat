import java.net.*;
import java.io.*;

public class ClientHandler implements Runnable {
    //****************************************
    private final Socket connectionSocket;
    private final ChatServer server;
    //****************************************
    public ClientHandler(Socket connectionSocket, ChatServer svr) {
        this.connectionSocket = connectionSocket;
        this.server = svr;
    }
    //****************************************
    public void send(String message, Socket clientSocket) {
        try {
            //Writer out = new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream()));
            PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);
            out.println(message);
        } catch(IOException e) {
            //nic tu nie ma
        }
    }
    //****************************************
    @Override
    public void run() {
        try {
            BufferedReader in = new BufferedReader(new InputStreamReader(connectionSocket.getInputStream()));
            String userInput;
            while(true) {
                userInput = in.readLine();
                if(userInput != null) {
                    System.out.println(userInput);
                    
                    if(userInput.startsWith(".bye")) {
                        break;
                    }
                    
                    if(userInput.startsWith(".stop")) {
                        server.setStopped(true);
                        server.stop();
                        break;
                    }
                    
                    server.syncSendToAll(userInput);
                } else {
                    break;
                }
            }
        } catch(IOException e) {
            System.out.println("NO WYWALIŁO NO");
        }
          
        server.removeClient(this.connectionSocket);
    }
    //****************************************
}
