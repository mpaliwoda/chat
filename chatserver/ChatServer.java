import java.net.*;
import java.io.*;
import java.util.*;

public class ChatServer {
    //****************************************
    private final static int PORT = 2048;
    private ServerSocket serverSocket = null;
    private ArrayList<Socket> connectionsList = new ArrayList<>();
    private boolean stopped = false;
    //****************************************
    public boolean isStopped() {
        return this.stopped;
    }
    //****************************************
    public void setStopped(boolean value) {
        this.stopped = value;
    }
    //****************************************
    public void stop() {
        try {
            this.serverSocket.close();
        } catch(IOException e) {           
        }
    }
    //****************************************
    private void sendHelper(String message, Socket clientSocket) {
        try {
            //Writer out = new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream())); //PrintWriter lepszy (println)
            PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);
            out.println(message);
        } catch(IOException e) {
            //nic tu nie ma
        }
    }
    //****************************************
    public void syncSendToAll(String userInput) {    
        synchronized(this.connectionsList){
            for(Socket clientSocket : this.connectionsList) {
                sendHelper(userInput, clientSocket);
            }
        }
    }
    //****************************************
    public void removeClient(Socket connectionSocket) {
        synchronized(this.connectionsList){
            this.connectionsList.remove(connectionSocket);
        }
    }
    //****************************************
    public static void main(String[] args) throws IOException {
        ChatServer srv = new ChatServer();
        srv.serverSocket = new ServerSocket(ChatServer.PORT);
        
        try {
            while (!srv.isStopped()) {
                Socket connection = srv.serverSocket.accept();
                synchronized(srv.connectionsList){
                    srv.connectionsList.add(connection);
                }
                Thread t = new Thread(new ClientHandler(connection, srv));
                t.start();
            }
        } catch(SocketException e) {
            //nic tu nie ma
        }
        
        if(!srv.isStopped())
            srv.serverSocket.close();
    }
    //****************************************
}