/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chatclient;
import java.net.*;
import java.io.*;

public class ChatClient implements Runnable {
    public static Socket clientSocket = null;
    private static Boolean stopWork = false;
    //****************************************
    public static synchronized void setStopWork(Boolean stopped) {
        ChatClient.stopWork = stopped;
    }
    //****************************************
    public static synchronized boolean getStopWork() {
        return ChatClient.stopWork;
    }
    //****************************************    
    public static void main(String[] args) throws UnknownHostException, IOException, InterruptedException {
        try {
            clientSocket = new Socket("localhost", 2048);
        } catch(UnknownHostException e) {
            System.out.println("Nieznany host");
            System.exit(1);
        } catch(IOException e) {
            System.out.println(e.getMessage());
            System.exit(2);
        }
        
        ChatClient chatClient = new ChatClient();
        Thread threadChatClient = new Thread(chatClient);
        
        ChatGenerateRandomOutput randomOutput = new ChatGenerateRandomOutput();
        Thread threadRandomOutput = new Thread(randomOutput);
        
        ChatReadKeyboard readKeyboard = new ChatReadKeyboard();
        Thread threadReadKeyboard = new Thread(readKeyboard);
        
        threadChatClient.start();
        threadRandomOutput.start();
        threadReadKeyboard.start();
        
        while(!ChatClient.getStopWork()) {
            Thread.sleep(300);
        }

        System.out.println("Closing socket");
        
        clientSocket.close();
        threadReadKeyboard.interrupt();
    }
    //****************************************
    @Override
    public void run() {
        BufferedReader in;
        String userInput;
        try {
            in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            while(!ChatClient.getStopWork()) {
                userInput = in.readLine();
                if(userInput == null) {
                    break;
                }
                System.out.println(userInput);
            }
        } catch(Exception e) {
            System.out.println("ChatClient Exception.");//
        }
        System.out.println("Thread stopped.1");
        ChatClient.setStopWork(true);
        System.out.println("Thread stopped.2");
    }
}
