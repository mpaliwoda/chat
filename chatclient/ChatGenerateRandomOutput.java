/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chatclient;

import static chatclient.ChatClient.clientSocket;
import java.io.*;
import java.util.*;
/**
 *
 * @author marcin
 */
public class ChatGenerateRandomOutput implements Runnable {
    @Override
    public void run() {
        try {
            PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);
            String msg = "";
            while(!ChatClient.getStopWork()) {
                msg = UUID.randomUUID().toString();
                synchronized(ChatClient.clientSocket) {    
                    out.println(msg);
                }
                Thread.sleep(1000);    
            }
        } catch (Exception e) {
            System.out.println("ChatGenerateRandomOutput Exception");
        }
        ChatClient.setStopWork(true);
        System.out.println("ChatGenerateRandomOutput stopped");
    }
}
