/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chatclient;


import static chatclient.ChatClient.clientSocket;
import java.io.*;
import java.util.*;
/**
 *
 * @author marcin
 */
public class ChatReadKeyboard implements Runnable {
    
    @Override
    public void run() {
        try {
            PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);
            String msg = "";
            int c;
            while(!ChatClient.getStopWork()) {
                boolean canSend = false;
                
                while(System.in.available() > 0) {
                    c = System.in.read();
                    
                    if(c == '\n') {
                        canSend = true;
                        break;
                    } 
                    msg += (char) c;
                }
                
                if(canSend) {
                    synchronized(ChatClient.clientSocket) {    
                        out.println(msg);
                    }
                    if(msg.startsWith(".bye")){
                        break;
                    }
                    if(msg.startsWith(".stop")){
                        break;
                    }
                    msg = "";
                }
            }
            System.out.println("Koniec petli.");
        } catch(Exception e) {
            System.out.println("ChatReadKeyboard Exception");
        }
        ChatClient.setStopWork(true);
        System.out.println("ChatReadKeyboard stopped");
    }

}
